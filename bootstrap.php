<?php declare(strict_types=1);

/*
 * (c) Subbkov <subbkov@mail.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use SubbkovOpenSource\PhpRandom as pr;
use SubbkovOpenSource\PhpRandom\Exception\PhpRandomException;

if (PHP_VERSION_ID >= 70200) {
    if (!function_exists('rand_int_exclude')) {
        /**
         * @param int              $min
         * @param int              $max
         * @param array|array<int> $exclusion
         *
         *
         * @throws PhpRandomException
         * @throws Exception
         *
         * @return int
         */
        function rand_int_exclude(int $min, int $max, array $exclusion = []): int
        {
            return pr\PhpRandom::rand_int_exclude($min, $max, $exclusion);
        }
    }

    if (!function_exists('rand_ints_exclude')) {
        /**
         * @param int              $min
         * @param int              $max
         * @param int              $num
         * @param array|array<int> $exclusion
         *
         * @throws PhpRandomException
         * @throws Exception
         *
         * @return array|array<int>
         */
        function rand_ints_exclude(int $min, int $max, int $num, array $exclusion = []): array
        {
            return pr\PhpRandom::rand_ints_exclude($min, $max, $num, $exclusion);
        }
    }
}
