<?php declare(strict_types=1);

/*
 * (c) Subbkov <subbkov@mail.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SubbkovOpenSource\PhpRandom;

use ArrayIterator;
use Exception;
use SubbkovOpenSource\PhpRandom\Exception\PhpRandomException;

/**
 * @author Subbkov <subbkov@mail.ru>
 */
final class PhpRandom
{
    /**
     * @param int              $min
     * @param int              $max
     * @param array|array<int> $exclusion
     *
     * @throws PhpRandomException
     * @throws Exception
     *
     * @return int
     */
    public static function rand_int_exclude(int $min, int $max, array $exclusion = []): int
    {
        $exclusion = \array_unique($exclusion);

        if (0 >= ($max - $min + 1) - \count($exclusion)) {
            throw new PhpRandomException(
                'The number of exceptions is greater than or equal to the range for a random number'
            );
        }

        $arrayIterator = new ArrayIterator(\array_values(\array_diff(\range($min, $max), $exclusion)));

        return $arrayIterator->offsetGet((int) \random_int(0, $arrayIterator->count()));
    }

    /**
     * @param int              $min
     * @param int              $max
     * @param int              $num
     * @param array|array<int> $exclusion
     *
     * @throws PhpRandomException
     * @throws Exception
     *
     * @return array|array<int>
     */
    public static function rand_ints_exclude(int $min, int $max, int $num, array $exclusion = []): array
    {
        $result = [];

        if (0 > $num) {
            throw new PhpRandomException('Value \'$num\' cannot be less than zero');
        }

        $exclusion = \array_unique($exclusion);

        if (0 >= ($max - $min + $num + 1) - \count($exclusion)) {
            throw new PhpRandomException(
                'The number of exceptions is greater than or equal to the range for a random number'
            );
        }

        if (0 === $num) {
            return $result;
        }

        $counter = 1;
        while ($counter <= $num) {
            $number      = self::rand_int_exclude($min, $max, $exclusion);
            $result[]    = $number;
            $exclusion[] = $number;

            echo $counter++;
        }

        return $result;
    }
}
