<?php declare(strict_types=1);

/*
 * (c) Subbkov <subbkov@mail.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SubbkovOpenSource\PhpRandom\Exception;

use Exception;

class PhpRandomException extends Exception
{
}
